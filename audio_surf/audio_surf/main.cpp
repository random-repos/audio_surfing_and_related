//TODO list
//add wall object
//design and add event handler
//do surface class
//level editor



#include "ENG_glob.h"
#include "ENG_container.h"
#include "ENG_graph.h"

#include <math.h>

extern EngineGlobals engine;

int main(int argc, char *argv[])
{

	//blank the screen for loading
	SDL_FillRect(engine.screen, NULL, 0xffffff);

	masterContainer * control = new masterContainer(engine.screen);

	bool quit = false;

	while(!quit)
	{
		if(!control->runControl())
			quit = true;

		//render screen
		SDL_Flip(engine.screen);

		if(engine.time.get_last() != 0)
			engine.errorLog.log( 1000/engine.time.get_last() );

		//update the time
		engine.time.update();
	}

	return 0;
}


