#ifndef AUD_H
#define AUD_H

#include "RtAudio.h"
#include "ENG_display.h"
#include "ENG_glob.h"
#include "ENG_camera.h"
#include "ENG_methods.h"

extern EngineGlobals engine;

const unsigned int BUFFER_FRAMES = 1024;


int record( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
         double streamTime, RtAudioStreamStatus status, void *userData );

//our mic object for audio input
class MicObject : public HitObject
{
public:
	MicObject();
	~MicObject();
private:
	RtAudio adc;
};

class SoundWave : public HitObject
{
public:
	SoundWave();
	~SoundWave();

	//common methods, handle input  excluded
	//update gets mic input from engine.mic_input
	virtual void update();
	virtual void draw(Camera * drawCam);

	//returns a very basic hit box
	//recordHit function will give moar details
	virtual SDL_Rect returnHit() { return m_rect; };
protected:
	int lastUpdate;
	SDL_Surface * m_mainSurface;

};


#endif