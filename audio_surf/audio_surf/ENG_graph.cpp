#include "ENG_graph.h"


AnimGraph::AnimGraph(string objectName)
{
	//all xmldata stored in one place
	//we should probably change this to global constants
	XMLNode mainNode = XMLNode::openFileHelper("testxml.xml","objects");
	XMLNode	objectNode = mainNode.getChildNodeWithAttribute("sprite","name",objectName.c_str());

	//if(m_graphs.find(objectName) == m_graphs.end())


	//if object node is not empty
	if(!objectNode.isEmpty())
		loadFrames(objectNode);

	lastFrameUpdate = 0;
};

AnimGraph::~AnimGraph()
{
	deleteNodes(m_currentFrame);
};

void AnimGraph::deleteNodes(Frame * node)
{

	if(m_currentFrame != NULL)
	{
		set<Frame *> visited;
		stack<Frame *> stack;

		//push starting node onto stack and visited list
		stack.push(node);
		visited.insert(node);

		while(!stack.empty())
		{

			//pop off top of stack so we can add children and delete it.
			Frame * temp = stack.top();
			stack.pop();
			
			//go through each of children
			for(map<animState, Frame*>::iterator it = temp->map.begin(); it != temp->map.end(); it++)
			{
				//if child is not NULL
				if(it->second != NULL)
				{
					//if we haven't visited the node before
					if(visited.find(it->second) == visited.end())
					{
						//push it into stack
						stack.push(it->second);
						//add it to visited list
						visited.insert(it->second);
					}
				}
			}
			//now delete the node we just finished processing processing
			delete temp;
		}
	}
};



bool AnimGraph::locate(animState state)
{
	map<animState, Frame*>::iterator it;
	it = m_currentFrame->map.find(state);
	if(it == m_currentFrame->map.end())
		return false;
	else
		return true;
}

bool AnimGraph::grabFrame(const int newTime, const animState state, Frame * & changeFrame)
{
	bool flag = false;
	if(lastFrameUpdate == 0)
		lastFrameUpdate = newTime;

	//set frames to render
	while(newTime-lastFrameUpdate >= m_currentFrame->time)
	{
		//advance time
		lastFrameUpdate += m_currentFrame->time;
		//if we take special path

		if(locate(state))
		{
			m_currentFrame = m_currentFrame->map[state];

			flag = true;
		}
		//else go default path
		else
		{
			m_currentFrame = m_currentFrame->map[DEFAULT];
		}
	}

	changeFrame = m_currentFrame;

	return flag;
};

bool AnimGraph::loadFrames(XMLNode objectNode)
{
	//set it up
	list<Frame *> * tempFrameList = new list<Frame *>;
	m_currentFrame = setUpFrames(&objectNode,1,tempFrameList);
	delete tempFrameList;

	return true;
};
