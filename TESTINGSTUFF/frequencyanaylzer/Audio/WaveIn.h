#if !defined WAVEIN_H
#define WAVEIN_H
//------------------------------------
//  Reliable Software, (c) 1996-2002
//------------------------------------
#include "AsyncDevice.h"
#include <windows.h>
#include <mmsystem.h>

namespace Wave
{
	class Format;

	class InDevice: public AsyncDevice
	{
	public:
		InDevice (Wave::Format const & format, unsigned devId = DEFAULT_DEVICE);
		~InDevice ();
		void Open (Win::Event & event);
		void Close ();
		void Start ();
		void Stop ();
		void Prepare (Wave::Header * header);
		void Unprepare (Wave::Header * header);
		void SendBuffer (Wave::Header * header);

		void Reset ();
		bool Ok () { return _status == MMSYSERR_NOERROR; }
		bool IsInUse () { return _status == MMSYSERR_ALLOCATED; }
		unsigned long GetSamplePosition ();
		unsigned GetError () { return _status; }
		void     GetErrorText (char* buf, int len);
	private:
		unsigned		_devId;
		HWAVEIN			_handle;
		MMRESULT		_status;
	};
}
#endif
