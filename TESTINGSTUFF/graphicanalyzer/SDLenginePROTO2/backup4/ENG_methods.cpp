#include "ENG_glob.h"
#include "ENG_methods.h"

SDL_Rect getRect(Uint16 w, Uint16 h, Sint16 x, Sint16 y)
{
	SDL_Rect temp;
	temp.x = x;
	temp.y = y;
	temp.h = h;
	temp.w = w;
	return temp;
}
unsigned int returnDiff(int one, int two) 
{ 
	return abs(one-two); 
};

//BLIT METHODS
void applySurface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip )
{
    //Make a temporary rectangle to hold the offsets
    SDL_Rect offset;
    
    //Give the offsets to the rectangle
    offset.x = x;
    offset.y = y;
    
    //Blit the surface
    SDL_BlitSurface( source, clip, destination, &offset );
}

//IMAGE METHODS
SDL_Surface * loadImage( string filename, Uint32 colorkey ) 
{
    //temp storage space
    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    
    //load imaeg
    loadedImage = IMG_Load( filename.c_str() );

    //If nothing went wrong in loading the image
   if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );

		//set color key
		if( optimizedImage != NULL ) 
		{
			//if no color key is set then make it white
			if(colorkey == 0)
				colorkey = SDL_MapRGB( optimizedImage->format, 0xFF, 0xFF, 0xFF ); 
			//sets the color key
			SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey ); 
		}
        
        //Free the old image
        SDL_FreeSurface( loadedImage );
    }

    //Return the optimized image
    return optimizedImage;
}


Uint32 get_pixel32( SDL_Surface *surface, int x, int y )
{
    //Convert the pixels to 32 bit
    Uint32 *pixels = (Uint32 *)surface->pixels;

    //Get the requested pixel
    return pixels[( y * surface->w ) + x];
}

void put_pixel32( SDL_Surface *surface, int x, int y, Uint32 pixel )
{
    //Convert the pixels to 32 bit
    Uint32 *pixels = (Uint32 *)surface->pixels;
    
    //Set the pixel
    pixels[( y * surface->w ) + x] = pixel;
}

//1 for horizontal 2 for vectical
SDL_Surface * flipSurface( SDL_Surface *surface, int flags )
{
    //Pointer to the soon to be flipped surface
    SDL_Surface *flipped = NULL;
    
    //If the image is color keyed
    if( surface->flags & SDL_SRCCOLORKEY )
    {
        flipped = SDL_CreateRGBSurface( SDL_SWSURFACE, surface->w, surface->h, surface->format->BitsPerPixel, surface->format->Rmask, surface->format->Gmask, surface->format->Bmask, 0 );
    }
    //Otherwise
    else
    {
        flipped = SDL_CreateRGBSurface( SDL_SWSURFACE, surface->w, surface->h, surface->format->BitsPerPixel, surface->format->Rmask, surface->format->Gmask, surface->format->Bmask, surface->format->Amask );
    }
    
    //If the surface must be locked
    if( SDL_MUSTLOCK( surface ) )
    {
        //Lock the surface
        SDL_LockSurface( surface );
    }

    //Go through columns
    for( int x = 0, rx = flipped->w - 1; x < flipped->w; x++, rx-- )
    {
        //Go through rows
        for( int y = 0, ry = flipped->h - 1; y < flipped->h; y++, ry-- )
        {
            //Get pixel
            Uint32 pixel = get_pixel32( surface, x, y );
            
            //Copy pixel
            if( ( flags & 1 ) && ( flags & 2 ) )
            {
                put_pixel32( flipped, rx, ry, pixel );
            }
            else if( flags & 2 )
            {
                put_pixel32( flipped, rx, y, pixel );
            }
            else if( flags & 1 )
            {
                put_pixel32( flipped, x, ry, pixel );
            }
        }    
    }
    
    //Unlock surface
    if( SDL_MUSTLOCK( surface ) )
    {
        SDL_UnlockSurface( surface );
    }

    //Copy color key
    if( surface->flags & SDL_SRCCOLORKEY )
    {
        SDL_SetColorKey( flipped, SDL_RLEACCEL | SDL_SRCCOLORKEY, surface->format->colorkey );
    }
    
    //Return flipped surface
    return flipped;
}



//TIMER CLASS
Timer::Timer()
{
	reset();
}

void Timer::reset()
{
	startTicks = SDL_GetTicks();
	updateTicks = 0;
	isPause = false;
}

void Timer::update()
{
	updateTicks = SDL_GetTicks();
}

void Timer::pause()
{
	if(!isPause)
	{
		pauseTicks = SDL_GetTicks();
		isPause = true;
	}
}

void Timer::unpause()
{
	if(isPause)
	{
		isPause = false;
		updateTicks = SDL_GetTicks() - (pauseTicks - updateTicks);
		startTicks += pauseTicks-updateTicks;
	}
}

int Timer::get_ticks()
{
	return SDL_GetTicks() - startTicks;
}

int Timer::get_last()
{
	return SDL_GetTicks()-updateTicks; 
}