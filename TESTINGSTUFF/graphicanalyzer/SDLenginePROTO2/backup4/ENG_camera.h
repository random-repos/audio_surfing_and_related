#ifndef CAMERA_H
#define CAMERA_H

#include "ENG_glob.h"
#include "ENG_display.h"
#include <cmath>

class DisplayObject;

class Camera 
{
public:
	Camera(SDL_Surface * screen);

	void update();

	//all sorta fancy camera moving functions :D
	//force moves camera to position
	void forceMove(int x, int y);
	//centers camera on sprite
	void center(DisplayObject & obj);
	//force centers ignoring boundaries
	void forceCenter(DisplayObject * obj);
	//camera follows sprite 
	void follow(DisplayObject * obj);
	
	//EFFECTS
	void shake(int degree);

	//Camera position
	SDL_Rect m_rect;
	//max dimension camera is allowed to travel
	SDL_Rect m_maxDim;

	//screen
	SDL_Surface * m_screen;

private:
	//checks if crossing boundary
	void checkBoundary();
};
#endif