#ifndef CONTAINER_H
#define CONTAINER_H

#include "ENG_glob.h"
#include "ENG_camera.h"
#include "ENG_display.h"
#include <list>

extern EngineGlobals engine;

class Container
{
public:
	Container(SDL_Surface * newScreen);
	~Container();

	//basic displayObject methods
	virtual void handleInput(SDL_Event & keyEvent, Uint8 * keystates);
	virtual void update();
	virtual void draw();

	//displayObject creation/destruction
	//addObject(identifier, xmltype)
	void deleteObject(IdStruct identifier);
	void deleteObject(list<DisplayObject*>::iterator it);
	void deleteAllObjects();

	//gives a quick surface to play with
	SDL_Surface * quickSurface();

	//accessors
	SDL_Surface * getSurface() { return m_screen; };

protected:
	//local screen
	SDL_Surface * m_screen;

	//Camera
	Camera m_camera;

	//object list
	list<DisplayObject *> activeObjects;
};

//ALIAS control (maybe program it in lol)
class masterContainer : public Container
{
public:
	masterContainer(SDL_Surface * newScreen);

	bool runControl();
	void renderObjects();
private:
	//for inputs
	SDL_Event keyEvent;
	Uint8 * keystates;
};

masterContainer::masterContainer(SDL_Surface *newScreen):Container(newScreen)
{
	
};

bool masterContainer::runControl()
{
	//handle events and get keystates
	//PollEvent automatically pumps for SDL_GetKeyState
	SDL_PollEvent( &keyEvent );
	keystates = SDL_GetKeyState( NULL ); 
	//also test for full screen
	if(keystates[SDLK_LALT] && keystates[SDLK_RETURN])
		engine.fullscreen();
	//test for quit
	if(keystates[SDLK_ESCAPE])
		return false;

	//LOADING ROUTINE
	if(activeObjects.empty())
	{
		XMLNode tempNode = XMLNode::openFileHelper("testxml.xml","objects");
		

		//write a destructor!

		DisplayObject * temp = new PlayerSprite( tempNode );

		DisplayObject * temp2 = new GenericSprite( tempNode );
		DisplayObject * temp3 = new GenericSprite( tempNode );
		DisplayObject * temp4 = new GenericSprite( tempNode );
		DisplayObject * temp5 = new GenericSprite( tempNode );

		

		((HitObject*)temp)->teleport(50,-500);

		((HitObject*)temp2)->teleport(0,-100);
		((HitObject*)temp3)->teleport(100,-100);
		((HitObject*)temp4)->teleport(200,-100);
		((HitObject*)temp5)->teleport(300,-100);

		activeObjects.push_back( temp );
		activeObjects.push_back( temp2 );
		activeObjects.push_back( temp3 );
		activeObjects.push_back( temp4 );
		activeObjects.push_back( temp5 );

		((PlayerSprite *)activeObjects.front())->inControl = true;
		m_camera.forceCenter(activeObjects.front());
		
	}

	//blank the screen
	SDL_FillRect(m_screen, NULL, 0xffffff);
	//control loop
	renderObjects();

	return true;
};


//GET RID OF THIS!!!!
//collission stuff
bool checkCollision( SDL_Rect& A, SDL_Rect& B )
{
    //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = A.x;
    rightA = A.x + A.w;
    topA = A.y;
    bottomA = A.y + A.h;
        
    //Calculate the sides of rect B
    leftB = B.x;
    rightB = B.x + B.w;
    topB = B.y;
    bottomB = B.y + B.h;
            
    //If any of the sides from A are outside of B
    if( bottomA <= topB )
        return false;
    if( topA >= bottomB )
        return false;    
    if( rightA <= leftB )
        return false;
    if( leftA >= rightB )
        return false;

    //If none of the sides from A are outside B
    return true;
}

bool checkcollision( HitObject * A, HitObject * B )
{
	return checkCollision(A->returnHit(), B->returnHit());
};




void masterContainer::renderObjects()
{

	//m_camera.forceCenter(activeObjects.front());
	m_camera.follow(activeObjects.front());
	//m_camera.shake(5);
	handleInput(keyEvent,keystates);
	update();

	//collision detection
	//BUG, each collision detected twice.
	//collision
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		//look for stage
		for(list<DisplayObject*>::iterator it2 = activeObjects.begin(); it2 != activeObjects.end(); it2++)
		{

			//if the two objects are not the same
			if(it2 != it)
				if( checkcollision( (HitObject *)*it, (HitObject *)(*it2) ) )
				{
					//cout << "HIT!" << endl;
					((HitObject *)(*it))->recordHit( ((HitObject*)(*it2) ) );
				}
		}
	}



	//event handling
	draw();
};

Container::Container(SDL_Surface * newScreen):m_camera(newScreen)
{
	m_screen = newScreen;
}

Container::~Container()
{
	deleteAllObjects();
}

//object routines
void Container::handleInput(SDL_Event & keyEvent, Uint8 * keystates)
{
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		(*it)->handleInput(keyEvent, keystates);
	}
}

void Container::update()
{
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		(*it)->update();
	}
}

void Container::draw()
{
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		(*it)->draw(&m_camera);
	}
};


//destructor
void Container::deleteAllObjects()
{
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		delete (*it);
	}
	activeObjects.clear();
}

void Container::deleteObject(IdStruct identifier)
{
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		if( (*it)->getId().id == identifier.id && (*it)->getId().type == identifier.type)
		{
			deleteObject(it);
			break;
		}
	}
};

void Container::deleteObject(list<DisplayObject*>::iterator it)
{
	delete *it;
	activeObjects.erase(it);
};

SDL_Surface * Container::quickSurface()
{
	return SDL_CreateRGBSurface( SDL_SWSURFACE, m_screen->w, m_screen->h, m_screen->format->BitsPerPixel, m_screen->format->Rmask, m_screen->format->Gmask, m_screen->format->Bmask, 0 );
}

#endif