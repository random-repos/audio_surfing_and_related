#ifndef METHODS_H
#define METHODS_H

//SOME UTILITY FUNCTIONS
SDL_Rect getRect(Uint16 w, Uint16 h, Sint16 x = 0, Sint16 y = 0);
unsigned int returnDiff(int one, int two);

//BLIT METHODS
void applySurface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip );

//IMAGE METHODS
SDL_Surface * loadImage( string filename, Uint32 colorkey = 0);
//direct access function
void put_pixel32( SDL_Surface *surface, int x, int y, Uint32 pixel );
//flips surface, 1 for horizontal, 2 for vertical, 1 & 2 for both.
SDL_Surface *flipSurface( SDL_Surface *surface, int flags );


//ERROR LOGGING CLASS
//you can not pass this class as an argument so don't try!
class Logger
{
public:
	Logger(string filename)	{	m_log.open( filename.c_str(), ios::out);	}
	~Logger() { m_log.close(); }
	void log(string message) {	m_log << message << endl; }
	void log(double message) { m_log << message << endl; }
private:
	ofstream m_log;
};

//The timer
class Timer
{
    private:
    //The clock time when the timer started
    int startTicks;
	//the ticks stored since update was last called.
	int updateTicks;
	//time when timer was paused
	int pauseTicks;
	//is it paused
	bool isPause;
    
    public:
    //Initializes variables
    Timer();
    
    //The various clock actions
	void reset();
	void update();

	//pausing and restarting
	//HAVE NOT BEEN TESTED BUT WHAT COULD GO WRONG...
	void pause();
	void unpause();
    
    //Gets the timer's time
    int get_ticks();

	//gets time since last update
	int get_last();
};


#endif