#ifndef GRAPH_H
#define GRAPH_H

#include "ENG_glob.h"
#include "ENG_display.h"
#include "xmlParser.h"
#include <map>
#include <list>
#include <string>


class animGraph
{
public:
	animGraph(string objectName);
	~animGraph(){};

	//returns true if state changes
	bool grabFrame(const int newTime, const animState state, Frame * changeFrame);
	
private:
	static map<string, Frame *> m_graphs;
	bool loadFrames(XMLNode objectNode);
	bool locate(animState state);



	Frame * m_currentFrame;
	int lastFrameUpdate;
};

animGraph::animGraph(string objectName)
{
	//all xmldata stored in one place
	//we should probably change this to global constants
	XMLNode mainNode = XMLNode::openFileHelper("testxml.xml","objects");
	XMLNode	objectNode = mainNode.getChildNodeWithAttribute("sprite","name",objectName.c_str());

	//if object node is not empty
	if(!objectNode.isEmpty())
		loadFrames(objectNode);
};

bool animGraph::locate(animState state)
{
	map<animState, Frame*>::iterator it;
	it = m_currentFrame->map.find(state);
	if(it == m_currentFrame->map.end())
		return false;
	else
		return true;
}

bool animGraph::grabFrame(const int newTime, const animState state, Frame *changeFrame)
{
	bool flag = false;

	//set frames to render
	while(newTime-lastFrameUpdate >= m_currentFrame->time)
	{
		//advance time
		lastFrameUpdate += m_currentFrame->time;
		//if we take special path
		if(locate(state))
		{
			m_currentFrame = m_currentFrame->map[state];

			flag = true;
		}
		//else go default path
		else
		{
			m_currentFrame = m_currentFrame->map[DEFAULT];
		}
	}

	changeFrame = m_currentFrame;

	return flag;
};

bool animGraph::loadFrames(XMLNode objectNode)
{
	//set it up
	list<Frame *> * tempFrameList = new list<Frame *>;
	m_currentFrame = setUpFrames(&objectNode,1,tempFrameList);
	delete tempFrameList;

	return true;
};

#endif