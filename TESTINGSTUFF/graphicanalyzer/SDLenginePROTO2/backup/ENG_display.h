//future additions
//scriptable sprite?

#ifndef DISPLAY_H
#define DISPLAY_H

#include "ENG_glob.h"
#include "ENG_camera.h"
#include "ENG_methods.h"
#include "xmlParser.h"
#include <map>
#include <string>
#include <list>

class Camera;

//identifier struct
struct IdStruct
{
	//it is level editor's job to make sure no objects have the same id
	string type;
	int id;
};

//specific to playerSprite class for now
enum animState
{
	DEFAULT,
	STAND,
	RUN,
	JUMP,
	CROUCH,
	STANDUP
};

//specific to playerSprite
//frame graph node
struct Frame
{
	unsigned int id;

	//rect
	SDL_Rect rectDraw;
	SDL_Rect rectHit;

	//time to spend on this animation
	int time; 

	//map of string to Frame pointer...
	map<animState, Frame*> map;
};

//some functions

//temporary surface management function
//turn this into a class
void getSurface(string filename, SDL_Surface * & main, SDL_Surface * & flip, bool freeSurface = false);
//gives a map from strings to animState enum
map<string,animState> * animStateMap();
//sets up frames... probably make a class out of this
Frame * setUpFrames(XMLNode * data, int id, list<Frame*> * visited);
//standard hit detection. return corrected hit box
SDL_Rect standardHit(const SDL_Rect & ourRect, const SDL_Rect & hitRect);


//location and velocity
struct LocVel { double x, y, xVel, yVel; bool dir; };

//is just an object. may or may not make use of any or all routines
class DisplayObject
{
public:
	//probably do nothing
	DisplayObject() {};
	~DisplayObject() {};

	//identifiers
	IdStruct getId() { return m_id; };

	//common methods
	virtual void update() {};
	virtual void handleInput(SDL_Event & keyEvent, Uint8 * keystates = NULL){};
	virtual void draw(Camera * drawCam) {};

protected:
	IdStruct m_id;

};

//these objects have hit detection added
//also adds location
class HitObject : public DisplayObject 
{
public:
	HitObject() { m_rect.x = m_rect.y = m_rect.h = m_rect.w = 0; }; 
	virtual void recordHit(){};
	virtual SDL_Rect returnHit() { return m_rect; };
	virtual void teleport(int x, int y) { m_rect.x = x; m_rect.y = y; };
protected:
	//this is object location 
	SDL_Rect m_rect;
};

//class sprite
//adds animation graph

//very specific derived class. Generalize later 
class GenericSprite : public HitObject 
{
public:
	GenericSprite(XMLNode data);
	~GenericSprite();

	//base routines
	virtual void handleInput(SDL_Event & keyEvent, Uint8 * keystates = NULL);
	virtual void draw(Camera * drawCam);
	virtual void update();

	//Hit detection stuff
	virtual SDL_Rect returnHit();
	virtual void recordHit(HitObject * obj);

	//teleports player to x, y
	virtual void teleport(int x, int y);

	//temp for screwing around
	bool inControl;

private:
	//location velocity direction
	LocVel m_locVel;

	//time state
	animState m_animState;
	animState m_actualState;
	int lastUpdate;
	int lastFrameUpdate;

	//graph
	Frame * m_currentFrame;
	bool locate();

	//source surfaces
	SDL_Surface * m_mainSurface;
	SDL_Surface * m_flipSurface;

	//draw routine helpers
	SDL_Surface * returnSurface() { return m_locVel.dir ? m_flipSurface :  m_mainSurface; };
	SDL_Rect returnDraw();

	//loading and deleting 
	bool loadFrames(XMLNode data);
	void deleteNodes(Frame * node);

	
};


//very specific derived class. Generalize later 
class PlayerSprite : public HitObject 
{
public:
	PlayerSprite(XMLNode data);
	~PlayerSprite();

	//base routines
	virtual void handleInput(SDL_Event & keyEvent, Uint8 * keystates = NULL);
	virtual void draw(Camera * drawCam);
	virtual void update();

	//Hit detection stuff
	virtual SDL_Rect returnHit();
	virtual void recordHit(HitObject * obj);

	//teleports player to x, y
	virtual void teleport(int x, int y);

	//temp for screwing around
	bool inControl;

private:
	//location velocity direction
	LocVel m_locVel;

	//time state
	animState m_animState;
	animState m_actualState;
	int lastUpdate;
	int lastFrameUpdate;

	//graph
	Frame * m_currentFrame;
	bool locate();

	//source surfaces
	SDL_Surface * m_mainSurface;
	SDL_Surface * m_flipSurface;

	//draw routine helpers
	SDL_Surface * returnSurface() { return m_locVel.dir ? m_flipSurface :  m_mainSurface; };
	SDL_Rect returnDraw();

	//loading and deleting 
	bool loadFrames(XMLNode data);
	void deleteNodes(Frame * node);

	
};


class WallObject : public HitObject
{
public:
private:
};



#endif