#include "ENG_display.h"
#include <list>
#include <map>
#include <set>
#include <stack>

extern EngineGlobals engine;

//setup graph
GenericSprite::GenericSprite(XMLNode data)
{
	loadFrames(data);
	lastUpdate = 0;
	lastFrameUpdate = 0;

	m_locVel.x = 0;
	m_locVel.y = 0;
	m_locVel.xVel = 0;
	m_locVel.yVel = 0;

	inControl = false;
};

bool GenericSprite::loadFrames(XMLNode data)
{
	//get sprite with attribute name = player
	XMLNode objectNode = data.getChildNodeWithAttribute("sprite","name","player");
	getSurface(objectNode.getAttribute("file"),m_mainSurface,m_flipSurface);

	list<Frame *> * tempFrameList = new list<Frame *>;
	m_currentFrame = setUpFrames(&objectNode,1,tempFrameList);
	delete tempFrameList;

	return true;
};

GenericSprite::~GenericSprite()
{
	deleteNodes(m_currentFrame);
};

void GenericSprite::deleteNodes(Frame * node)
{

	if(m_currentFrame != NULL)
	{
		set<Frame *> visited;
		stack<Frame *> stack;

		//push starting node onto stack and visited list
		stack.push(node);
		visited.insert(node);

		while(!stack.empty())
		{

			//pop off top of stack so we can add children and delete it.
			Frame * temp = stack.top();
			stack.pop();
			
			//go through each of children
			for(map<animState, Frame*>::iterator it = temp->map.begin(); it != temp->map.end(); it++)
			{
				//if child is not NULL
				if(it->second != NULL)
				{
					//if we haven't visited the node before
					if(visited.find(it->second) == visited.end())
					{
						//push it into stack
						stack.push(it->second);
						//add it to visited list
						visited.insert(it->second);
					}
				}
			}
			//now delete the node we just finished processing processing
			delete temp;
		}
	}
};

void GenericSprite::handleInput(SDL_Event &keyEvent, Uint8 *keystates)
{
};

void GenericSprite::update()
{
	int ticks  = engine.time.get_ticks();

	//set frames to render
	while(ticks-lastFrameUpdate >= m_currentFrame->time)
	{
		//advance time
		lastFrameUpdate += m_currentFrame->time;
		//if we take special path
		if(locate())
		{
			m_animState = m_actualState;
			m_currentFrame = m_currentFrame->map[m_animState];
		}
		//else go default path
		else
		{
			m_currentFrame = m_currentFrame->map[DEFAULT];
		}
	}

	//update the timer
	lastUpdate = ticks;
};

//teleports player to location x y
void GenericSprite::teleport(int x, int y)
{
	m_locVel.x = x;
	m_locVel.y = y;
};


SDL_Rect GenericSprite::returnHit()
{
	m_rect.x = (int)m_locVel.x;
	m_rect.y = (int)m_locVel.y;
	return m_rect;
};


void GenericSprite::draw(Camera * drawCam)
{
	applySurface(
		returnHit().x - drawCam->m_rect.x + drawCam->m_rect.w/2, 
		returnHit().y - drawCam->m_rect.y + drawCam->m_rect.h/2,
		returnSurface(),
		drawCam->m_screen,
		&returnDraw());
};

bool GenericSprite::locate()
{
	map<animState, Frame*>::iterator it;
	it = m_currentFrame->map.find(m_animState);
	if(it == m_currentFrame->map.end())
		return false;
	else
		return true;
}

SDL_Rect GenericSprite::returnDraw() 
{ 
	if(m_locVel.dir)
		return getRect( m_currentFrame->rectDraw.w, m_currentFrame->rectDraw.h, m_mainSurface->w - m_currentFrame->rectDraw.x - m_currentFrame->rectDraw.w, m_currentFrame->rectDraw.y);
	else
		return m_currentFrame->rectDraw;
};

void GenericSprite::recordHit(HitObject * obj)
{
	 SDL_Rect temp = standardHit(this->returnHit(), obj->returnHit());
	 m_locVel.x = temp.x;
	 m_locVel.y = temp.y;
};