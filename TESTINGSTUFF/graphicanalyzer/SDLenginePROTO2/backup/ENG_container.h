#ifndef CONTAINER_H
#define CONTAINER_H

#include "ENG_glob.h"
#include "ENG_camera.h"
#include "ENG_display.h"
#include <list>

extern EngineGlobals engine;

class Container
{
public:
	Container(SDL_Surface * newScreen);
	~Container();

	//basic displayObject methods
	virtual void handleInput(SDL_Event & keyEvent, Uint8 * keystates);
	virtual void update();
	virtual void draw();

	//displayObject creation/destruction
	//addObject(identifier, xmltype)
	void deleteObject(IdStruct identifier);
	void deleteObject(list<DisplayObject*>::iterator it);
	void deleteAllObjects();

	//gives a quick surface to play with
	SDL_Surface * quickSurface();

	//accessors
	SDL_Surface * getSurface() { return m_screen; };

protected:
	//local screen
	SDL_Surface * m_screen;

	//Camera
	Camera m_camera;

	//object list
	list<DisplayObject *> activeObjects;
};

//ALIAS control (maybe program it in lol)
class masterContainer : public Container
{
public:
	masterContainer(SDL_Surface * newScreen);

	bool runControl();
	void renderObjects();
private:
	//for inputs
	SDL_Event keyEvent;
	Uint8 * keystates;
};

masterContainer::masterContainer(SDL_Surface *newScreen):Container(newScreen)
{
	
};

bool masterContainer::runControl()
{
	//handle events and get keystates
	//PollEvent automatically pumps for SDL_GetKeyState
	SDL_PollEvent( &keyEvent );
	keystates = SDL_GetKeyState( NULL ); 
	//also test for full screen
	if(keystates[SDLK_LALT] && keystates[SDLK_RETURN])
		engine.fullscreen();
	//test for quit
	if(keystates[SDLK_ESCAPE])
		return false;

	//LOADING ROUTINE
	if(activeObjects.empty())
	{
		XMLNode tempNode = XMLNode::openFileHelper("testxml.xml","objects");
		DisplayObject * temp = new PlayerSprite( tempNode );
		DisplayObject * temp2 = new PlayerSprite( tempNode );
		activeObjects.push_back( temp );
		activeObjects.push_back( temp2 );
		((PlayerSprite *)activeObjects.front())->inControl = true;
		m_camera.forceCenter(activeObjects.front());
		
	}

	//blank the screen
	SDL_FillRect(m_screen, NULL, 0xffffff);
	//control loop
	renderObjects();

	return true;
};

void masterContainer::renderObjects()
{

	//m_camera.forceCenter(activeObjects.front());
	m_camera.follow(activeObjects.front());
	//m_camera.shake(5);
	handleInput(keyEvent,keystates);
	update();
	//collision detection
	//event handling
	draw();
};

Container::Container(SDL_Surface * newScreen):m_camera(newScreen)
{
	m_screen = newScreen;
}

Container::~Container()
{
	deleteAllObjects();
}

//object routines
void Container::handleInput(SDL_Event & keyEvent, Uint8 * keystates)
{
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		(*it)->handleInput(keyEvent, keystates);
	}
}

void Container::update()
{
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		(*it)->update();
	}
}

void Container::draw()
{
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		(*it)->draw(&m_camera);
	}
};


//destructor
void Container::deleteAllObjects()
{
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		delete (*it);
	}
	activeObjects.clear();
}

void Container::deleteObject(IdStruct identifier)
{
	for(list<DisplayObject*>::iterator it = activeObjects.begin(); it != activeObjects.end(); it++)
	{
		if( (*it)->getId().id == identifier.id && (*it)->getId().type == identifier.type)
		{
			deleteObject(it);
			break;
		}
	}
};

void Container::deleteObject(list<DisplayObject*>::iterator it)
{
	delete *it;
	activeObjects.erase(it);
};

SDL_Surface * Container::quickSurface()
{
	return SDL_CreateRGBSurface( SDL_SWSURFACE, m_screen->w, m_screen->h, m_screen->format->BitsPerPixel, m_screen->format->Rmask, m_screen->format->Gmask, m_screen->format->Bmask, 0 );
}

#endif